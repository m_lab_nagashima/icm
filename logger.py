import os
import pandas as pd

class Logger():
    _DF_TRIAL_COLUMNS = ['map_no', 'alpha' ,'round', 'steps', 'goal_rate', 'entropy', 'entropy_n']
    def __init__(self):
        pass

    def reset(self):
        self._map_nos = []
        self._alpha = []
        self._rounds = []
        self._steps = []
        self._goal_rates = []
        self._entropies = []
        self._entropies_n = []

    def save_recoding_trial_log(self, log_name):
        df = pd.DataFrame(
            data={
                'map_no': self._map_nos,
                'alpha': self._alpha,
                'round': self._rounds,
                'steps' : self._steps,
                'goal_rate': self._goal_rates,
                'entropy': self._entropies,
                'entropy_n': self._entropies_n,
            },
            columns = Logger._DF_TRIAL_COLUMNS
        )

        if os.path.exists(log_name) == True:
            df.to_csv(log_name, index=False, header=False, mode='a')
        else:
            df.to_csv(log_name, index=False, header=True, mode='w')

    def record_trial(self, map_no, alpha, round, step, goal_rate, entropy, entropy_n):
        self._map_nos.append(map_no)
        self._alpha.append(alpha)
        self._rounds.append(round)
        self._steps.append(step)
        self._goal_rates.append(goal_rate)
        self._entropies.append(entropy)
        self._entropies_n.append(entropy_n)