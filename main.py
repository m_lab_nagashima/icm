import os
import torch.multiprocessing as mp
from parallel_env import ParallelEnv
from logger import Logger

os.environ['OMP_NUM_THREADS'] = '1'

def learn(n_threads=4, run_num = 10, alpha=[0.1, 0.3, 0.5, 0.7, 0.9], env_sizes=[7 ,9 ,11], env_num_max=10 ):
    mp.set_start_method('spawn')
    n_actions = 4
    input_shape = [3, 42, 42]
    logger = Logger()

    for env_size in env_sizes:
        print('env_size={0}'.format(env_size))
        logger.reset()
        env_name = "{0}_{0}".format(env_size)
        for a in alpha:
            for env_num in range(0, env_num_max):
                print('alpha={0}, env_num={1}'.format(a, env_num))
                for _ in range(0, run_num):
                    ParallelEnv(env_name=env_name, env_size = env_size, env_num=env_num, num_threads=n_threads,
                                    n_actions=n_actions,
                                    input_shape=input_shape, limit_round_steps = 100, limit_total_steps = 3600, alpha=a, stop_threshold=5.0, logger=logger, icm=True)
        logger.save_recoding_trial_log(env_name + '.csv')

if __name__ == '__main__':
    learn()
    # mp.set_start_method('spawn')
    # n_threads = 4
    # n_actions = 4
    # input_shape = [3, 42, 42]
    # logger = Logger()
    # logger.reset()
    # for i in range(0, 10):
    #     env = ParallelEnv(env_name='11_11', env_num=0, num_threads=n_threads,
    #                       n_actions=n_actions,
    #                       input_shape=input_shape, limit_round_steps = 100, limit_total_steps = 3600, alpha=0.1, stop_threshold=5.0, logger=logger, icm=True)
    # logger.save_recoding_trial_log("test.log")
